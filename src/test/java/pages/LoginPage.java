package pages;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage {

    protected WebDriver driver;
    protected WebDriverWait wait;
    protected JavascriptExecutor jse;


    private By txt_username = By.xpath("//*[@id=\"username\"]");
    private By txt_password = By.xpath("//*[@id=\"Password\"]");
    private By btn_login = By.id("btnLogin");
    private By check_test = By.id("user_name_div");
//    private By txt_username = By.xpath("//*[@id=\"txtUsername\"]");
//    private By txt_password = By.xpath("//*[@id=\"txtPassword\"]");
//    private By btn_login = By.xpath("//*[@id=\"btnLogin\"]");
//    private By check_test = By.xpath("//*[@id=\"branding\"]/a[1]/img");


    public LoginPage(WebDriver driver) {
        this.driver = driver;

        if (driver.getTitle().equals("Home"))
            throw new IllegalStateException("This is not login page. The current page is" + driver.getCurrentUrl());
    }

    public void enterUserName(String userName) throws InterruptedException {

        Thread.sleep(2000);
        wait = new WebDriverWait(driver, 10);
        WebElement _username = wait.until(ExpectedConditions.elementToBeClickable(txt_username));
        System.out.println(userName);
        _username.sendKeys(userName);
    }

    public void enterPassword(String password) throws InterruptedException {
        Thread.sleep(2000);
//        driver.findElement(txt_password).sendKeys(password);
        wait = new WebDriverWait(driver, 10);
        WebElement _password = wait.until(ExpectedConditions.elementToBeClickable(txt_password));
        System.out.println(password);
        _password.sendKeys(password);
    }

    public void clickLogin() {
        jse = (JavascriptExecutor) driver;
        WebElement ele = driver.findElement(btn_login);
        jse.executeScript("arguments[0].click()", ele);
        System.out.println("click");
    }

    public void checkTestCase() throws InterruptedException {
        Thread.sleep(5000);
        System.out.println(driver.getTitle());
        driver.findElement(check_test).isDisplayed();
    }

}
