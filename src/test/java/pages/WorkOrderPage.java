package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;

public class WorkOrderPage {

    protected WebDriver driver;
    protected Select select;
    protected WebDriverWait wait;


    private By click_workOrder = By.xpath("/html/body/div/div[2]/div[2]/div/div/div/a[1]/div/div/p");
    private By check_tbl_displays = By.id("workOrder_main_table");
    private By tab_new_work_order = By.linkText("New Work Order");
    private By txt_verify_work_order_page = By.xpath("/html/body/div[1]/div[2]/div[1]/p");
    private By dd_marketing_ex = By.xpath("//*[@id=\"user_name\"]");
    private By dd_wo_type = By.xpath("//*[@id=\"wo_type_Dropdown\"]");
    private By txtbx_scheduleRef = By.id("txt_schedule_ref");
    private By txtbx_product = By.id("product-name");
    private By dd_agency = By.xpath("//*[@id=\"agencyDropdown\"]");
    private By dd_endClient = By.xpath("//*[@id=\"clientDropdown\"]");
    private By dd_billClientDropdown = By.xpath("//*[@id=\"billClientDropdown\"]");
    private By txt_bx_fromDate = By.xpath("//*[@id=\"form-date\"]/div/span[2]/span");
    private By fromDate = By.xpath("/html/body/div[8]/div/div[1]/table/tbody/tr[3]/td[3]");
    private By txt_bx_toDate = By.xpath("//*[@id=\"to-date\"]/div/span[2]/span");
    private By toDate = By.xpath("/html/body/div[9]/div/div[1]/table/tbody/tr[4]/td[6]");
    private By dd_revenue_month = By.xpath("//*[@id=\"revenue-month\"]");
    private By txt_bx_totalBudget = By.xpath("//*[@id=\"txt_total_budget\"]");
    private By txt_bx_entersBudget = By.xpath("//*[@id=\"txt_WithoutTax\"]");
    private By btn_entersBudget = By.xpath("//*[@id=\"budgetplan_view_popup\"]/div/div/div[2]/div/div/div[8]/div/button");
    private By txt_comments = By.xpath("//*[@id=\"comment\"]");
    private By dd_channelDropdown = By.xpath("//*[@id=\"channelDropdown\"]");
    private By txt_tvc_spots = By.xpath("//*[@id=\"txt_tvc_spots\"]");
    private By txt_duration = By.xpath("//*[@id=\"txt_duration\"]");
    private By txt_spots = By.xpath("//*[@id=\"txt_spots\"]");
    private By btn_spotcount_add = By.xpath("//*[@id=\"spotcount_view_popup\"]/div/div/div[2]/div[1]/div/div/div[3]/button");
    private By btn__spotcount_save = By.xpath("//*[@id=\"spotcount_view_popup\"]/div/div/div[2]/div[3]/div/div/button");
    private By ch_bonus = By.xpath("//*[@id=\"visible\"]");
    private By txt_tvc_value = By.xpath("//*[@id=\"txt_tvc_spots_packege\"]");
    private By txt_logo_spots = By.xpath("//*[@id=\"txt_logo_spots\"]");
    private By txt_logo_spots_b = By.xpath("//*[@id=\"txt_logo_spots_b\"]");
    private By txt_logos_spots_packege = By.xpath("//*[@id=\"txt_logos_spots_packege\"]");
    private By txt_Crawler_spots = By.xpath("//*[@id=\"txt_Crawler_spots\"]");
    private By txt_Crawler_spots_b = By.xpath("//*[@id=\"txt_Crawler_spots_b\"]");
    private By txt_crawler_spots_packege = By.xpath("//*[@id=\"txt_crawler_spots_packege\"]");
    private By txt_v_squeeze_spots = By.xpath("//*[@id=\"txt_v_squeeze_spots\"]");
    private By txt_v_squeeze_spots_b = By.xpath("//*[@id=\"txt_v_squeeze_spots_b\"]");
    private By txt_v_squeeze_spots_packege = By.xpath("//*[@id=\"txt_v_squeeze_spots_packege\"]");
    private By txt_l_squeeze_spots = By.xpath("//*[@id=\"txt_l_squeeze_spots\"]");
    private By txt_l_squeeze_spots_b = By.xpath("//*[@id=\"txt_l_squeeze_spots_b\"]");
    private By txt_l_squeeze_spots_packege = By.xpath("//*[@id=\"txt_l_squeeze_spots_packege\"]");
    private By btn_save_workOrder = By.xpath("/html/body/div[1]/div[2]/div[2]/div/div/div[4]/button");
    private By btn_add_divChannelDetails = By.xpath("//*[@id=\"divChannelDetails\"]/div[2]/button");
    private By txt_tvc_spots_b = By.xpath("//*[@id=\"txt_tvc_spots_b\"]");

    private By btn_addAdvertisment = By.xpath("//*[@id=\"new_workOrder_form\"]/div[11]/div/div/button");
    private By dd_addAdvertisment = By.xpath("//*[@id=\"advert_list_pop_div\"]/div/button/span[1]");
    private By selectAdvertisment = By.xpath("//*[@id=\"advert_list_pop_div\"]/div/div/ul/li[3]/a");
    private By btn_add = By.xpath("//*[@id=\"addAdvertisementModal\"]/div/div/div[2]/div/div[2]/button");
    private By btn_delete = By.xpath("//*[@id=\"tr_61\"]/td[7]/button/span");
    private By btn_save = By.xpath("//*[@id=\"addAdvertisementModal\"]/div/div/div[3]/button");



    public WorkOrderPage(WebDriver driver) {
        this.driver = driver;
    }

    public void addAdvertisment() throws InterruptedException {
        wait = new WebDriverWait(driver, 10);
        WebElement _addAdvertisment = wait.until(ExpectedConditions.elementToBeClickable(btn_addAdvertisment));
        _addAdvertisment.click();

        Thread.sleep(2000);

        WebElement d_addAdvertisment = wait.until(ExpectedConditions.elementToBeClickable(dd_addAdvertisment));
        d_addAdvertisment.click();

        Thread.sleep(1000);

        WebElement advertisment = wait.until(ExpectedConditions.elementToBeClickable(selectAdvertisment));
        advertisment.click();

        Thread.sleep(1000);

        WebElement _add = wait.until(ExpectedConditions.elementToBeClickable(btn_add));
        _add.click();

        Thread.sleep(1000);

        WebElement _delete = wait.until(ExpectedConditions.elementToBeClickable(btn_delete));
        _delete.click();

        Thread.sleep(1000);

        _add.click();

        Thread.sleep(1000);

        WebElement _save = wait.until(ExpectedConditions.elementToBeClickable(btn_save));
        _save.click();

    }


    public void navigateWorkOrderPage() {
        wait = new WebDriverWait(driver, 10);
        WebElement _workOrder = wait.until(ExpectedConditions.elementToBeClickable(click_workOrder));
        _workOrder.click();
    }

    public void checkTable() {
        wait = new WebDriverWait(driver, 10);
        WebElement _tbl_displays = wait.until(ExpectedConditions.elementToBeClickable(check_tbl_displays));
        _tbl_displays.click();
    }

    public void clickNewWorkOrder() {
        wait = new WebDriverWait(driver, 10);
        WebElement _new_work_order = wait.until(ExpectedConditions.elementToBeClickable(tab_new_work_order));
        _new_work_order.click();
    }

    public void verify_work_order_page() throws InterruptedException {
        Thread.sleep(2000);
        driver.findElement(txt_verify_work_order_page).isDisplayed();
    }

    public void marketing_ex() throws InterruptedException {
        Thread.sleep(2000);
        select = new Select(driver.findElement(dd_marketing_ex));
        select.selectByValue("Ashan_09074");
    }

    public void wo_type() throws InterruptedException {
        Thread.sleep(2000);
        select = new Select(driver.findElement(dd_wo_type));
        select.selectByValue("Trading");
    }

    public void schduleRef() throws InterruptedException {
        Thread.sleep(2000);
        driver.findElement(txtbx_scheduleRef).sendKeys("1700");
    }

    public void productName() throws InterruptedException {
        Thread.sleep(2000);
        driver.findElement(txtbx_product).sendKeys("1700");
    }

    public void agency() throws InterruptedException {
        Thread.sleep(2000);
        select = new Select(driver.findElement(dd_agency));
        select.selectByValue("48");
    }

    public void endClient() {
        select = new Select(driver.findElement(dd_endClient));
        select.selectByValue("267");
    }

    public void billClient() {
        select = new Select(driver.findElement(dd_billClientDropdown));
        select.selectByValue("592");
    }

    public void fromDate() throws InterruptedException {
        driver.findElement(txt_bx_fromDate).click();
        Thread.sleep(1000);
        driver.findElement(fromDate).click();
    }

    public void toDate() throws InterruptedException {
        driver.findElement(txt_bx_toDate).click();
        Thread.sleep(1000);
        driver.findElement(toDate).click();
    }

    public void revenueMonth() {
        select = new Select(driver.findElement(dd_revenue_month));
        select.selectByValue("November");
    }

    public void totalBudget() throws InterruptedException {
        driver.findElement(txt_bx_totalBudget).click();
        Thread.sleep(3000);
        driver.findElement(txt_bx_entersBudget).sendKeys("10000");
    }

    public void saveBudget() {
        driver.findElement(btn_entersBudget).click();
    }

//    public void addAdvertismnet(){}


    public void addComments() {
        driver.findElement(txt_comments).sendKeys("comment area");
    }

    public void channelDropdown() {
        select = new Select(driver.findElement(dd_channelDropdown));
        select.selectByValue("18|Star Plus");
    }

    public void click_tvc() {
        driver.findElement(txt_tvc_spots).click();
    }

    public void tvc_duration() {
        driver.findElement(txt_duration).sendKeys("30");
    }

    public void tvc_spots() {
        driver.findElement(txt_spots).sendKeys("3");
    }

    public void tvc_addBtn() {
        driver.findElement(btn_spotcount_add).click();
    }

    public void tvc_saveBtn() {
        driver.findElement(btn__spotcount_save).click();
    }

    public void check_bonus() {
        driver.findElement(ch_bonus).click();
    }

    public void setTxt_tvc_value() {
        driver.findElement(txt_tvc_value).sendKeys("500");
    }

    public void addBonusTVC() throws InterruptedException {
        driver.findElement(txt_tvc_spots_b).click();
        Thread.sleep(1000);
        driver.findElement(txt_duration).clear();
        Thread.sleep(1000);
        driver.findElement(txt_duration).sendKeys("10");
        Thread.sleep(1000);
        driver.findElement(txt_spots).clear();
        Thread.sleep(1000);
        driver.findElement(txt_spots).sendKeys("2");
        Thread.sleep(1000);
        driver.findElement(btn_spotcount_add).click();
        Thread.sleep(1000);
        driver.findElement(btn__spotcount_save).click();
    }

    public void setTxt_logo_spots() {
        driver.findElement(txt_logo_spots).sendKeys("3");
    }

    public void setTxt_logo_spots_b() {
        driver.findElement(txt_logo_spots_b).sendKeys("1");
    }

    public void setTxt_logos_spots_packege() {
        driver.findElement(txt_logos_spots_packege).sendKeys("100");
    }

    public void setTxt_Crawler_spots() {
        driver.findElement(txt_Crawler_spots).sendKeys("3");
    }

    public void setTxt_Crawler_spots_b() {
        driver.findElement(txt_Crawler_spots_b).sendKeys("1");
    }

    public void setTxt_crawler_spots_packege() {
        driver.findElement(txt_crawler_spots_packege).sendKeys("100");
    }

    public void setTxt_v_squeeze_spots() {
        driver.findElement(txt_v_squeeze_spots).sendKeys("3");
    }

    public void setTxt_v_squeeze_spots_b() {
        driver.findElement(txt_v_squeeze_spots_b).sendKeys("1");
    }

    public void setTxt_v_squeeze_spots_packege() {
        driver.findElement(txt_v_squeeze_spots_packege).sendKeys("100");
    }

    public void setTxt_l_squeeze_spots() {
        driver.findElement(txt_l_squeeze_spots).sendKeys("3");
    }

    public void setTxt_l_squeeze_spots_b() {
        driver.findElement(txt_l_squeeze_spots_b).sendKeys("1");
    }

    public void setTxt_l_squeeze_spots_packege() {
        driver.findElement(txt_l_squeeze_spots_packege).sendKeys("100");
    }

    public void setBtn_add_divChannelDetails() {
        driver.findElement(btn_add_divChannelDetails).click();
    }

    public void setBtn_save_workOrder() {
        driver.findElement(btn_save_workOrder).click();
    }
}
