package pages.scheduler;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;


public class ManualPlayList {

    protected WebDriver driver;
    protected WebDriverWait wait;
    protected JavascriptExecutor jse;


    public ManualPlayList(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//*[@id=\"includedMenubar\"]/nav[2]/div/ul/li[4]/a")
    WebElement zero_ads;
    @FindBy(xpath = "//*[@id=\"includedMenubar\"]/nav[2]/div/ul/li[4]/ul/li[2]/a")
    WebElement manual_channel;
    @FindBy(xpath = "/html/body/div[2]/div/div[1]/p")
    WebElement verify_manual_playList;
    @FindBy(xpath = "//*[@id=\"channel_list\"]")
    WebElement m_select_channel;
    @FindBy(xpath = "//*[@id=\"channel_list\"]/option[5]")
    WebElement channel_selected;

    @FindBy(xpath = "/html/body/div[2]/div/div[2]/div/div/div[1]/div[2]/button")
    WebElement btn_add_advertisment;
    @FindBy(xpath = "//*[@id=\"4\"]")
    WebElement advertisment1;
    @FindBy(xpath = "//*[@id=\"12\"]")
    WebElement advertisment2;
    @FindBy(xpath = "//*[@id=\"tbl_all_advert_previous\"]")
    WebElement previous;
    @FindBy(xpath = "//*[@id=\"tbl_all_advert_next\"]")
    WebElement next;
    @FindBy(xpath = "//*[@id=\"tbl_all_advert_paginate\"]/span/a[2]")
    WebElement forward_2;
    @FindBy(xpath = "//*[@id=\"addAdvertisement\"]/div/div/div[2]/div[3]/button")
    WebElement btn_add;

    @FindBy(xpath = "//*[@id=\"151\"]")
    WebElement btn_delete;
    @FindBy(xpath = "/html/body/div[2]/div/div[2]/div/div/div[3]/button")
    WebElement btn_save;
    private By delId = By.id("151");


    public void clickZeroAds() {
        jse = (JavascriptExecutor) driver;
        jse.executeScript("arguments[0].click()", zero_ads);
    }


    public void manual_channel() {
        wait = new WebDriverWait(driver, 10);
        WebElement m_channel = wait.until(ExpectedConditions.elementToBeClickable(manual_channel));
        m_channel.click();
    }

    public void setVerify_manual_playList() {
        verify_manual_playList.isDisplayed();
    }

    public void select_channel() throws InterruptedException {
        wait = new WebDriverWait(driver, 10);
        WebElement ms_channel = wait.until(ExpectedConditions.elementToBeClickable(m_select_channel));
        ms_channel.click();
        Thread.sleep(2000);
        WebElement _channel = wait.until(ExpectedConditions.elementToBeClickable(channel_selected));
        _channel.click();
    }


    public void add_advertisment() throws InterruptedException {
        btn_add_advertisment.click();
        Thread.sleep(3000);
    }

    public void select_advertisment() throws InterruptedException {
        Thread.sleep(3000);
        jse = (JavascriptExecutor) driver;
        jse.executeScript("arguments[0].click()", advertisment1);
        jse.executeScript("arguments[0].click()", advertisment2);
    }

    public void entries() throws InterruptedException {
        Thread.sleep(1000);
        next.click();
        Thread.sleep(1000);
        previous.click();
        Thread.sleep(1000);
        forward_2.click();
    }

    public void addAdvertisment() {
        jse = (JavascriptExecutor) driver;
        jse.executeScript("arguments[0].click()", btn_add);
    }

    public void deleteAdvertisment() {

        List<WebElement> idList = driver.findElements(By.id("tbl_zero_ads_manual"));
        ArrayList<String> deleteIDs = new ArrayList<String>();

        for (WebElement inputID : idList) {
            if (!inputID.getAttribute("id").equals(""))
                deleteIDs.add(inputID.getAttribute("id"));
        }

        int i = 1;
        for (String id : deleteIDs) {
            System.out.println("ID " + (i++) + ": " + id);
        }
    }

    public void save() {
        jse = (JavascriptExecutor) driver;
        jse.executeScript("arguments[0].click()", btn_save);
    }

}
