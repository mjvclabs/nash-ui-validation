package pages.scheduler;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class AutoPlayList {

    protected WebDriver driver;
    protected WebDriverWait wait;
    protected JavascriptExecutor jse;


    @FindBy(xpath = "//*[@id=\"includedMenubar\"]/nav[2]/div/ul/li[4]/a")
    WebElement zero_ads;
    @FindBy(xpath = "//*[@id=\"includedMenubar\"]/nav[2]/div/ul/li[4]/ul/li[1]/a")
    WebElement auto_play;
    @FindBy(xpath = "//*[@id=\"10\"]")
    WebElement delete_ads;

    @FindBy(xpath = "/html/body/div[2]/div/div[2]/div/div/div[2]/button[4]")
    WebElement copy_channel;
    @FindBy(xpath = "//*[@id=\"copyChannel\"]/div/div/div[1]/h4/p")
    WebElement verify_copy_channel;

    @FindBy(xpath = "//*[@id=\"cmb_copy_list\"]")
    WebElement dd_copy_channel;
    @FindBy(xpath = "//*[@id=\"cmb_copy_list\"]/option[3]")
    WebElement select_channel;
    @FindBy(xpath = "//*[@id=\"cmb_to_list\"]")
    WebElement copy_channel_to;
    @FindBy(xpath = "//*[@id=\"cmb_to_list\"]/option[7]")
    WebElement select_channel2;
    @FindBy(xpath = "//*[@id=\"copyChannel\"]/div/div/div[2]/form/div[3]/button")
    WebElement btn_copied;

    @FindBy(xpath = "/html/body/div[2]/div/div[2]/div/div/div[2]/button[3]")
    WebElement btn_new_channel;
    @FindBy(xpath = "//*[@id=\"addChannel\"]/div/div/div[1]/h4/p")
    WebElement n_verify_new_channel;
    @FindBy(xpath = "//*[@id=\"19_chan\"]")
    WebElement n_select_channel1;
    @FindBy(xpath = "//*[@id=\"22_chan\"]")
    WebElement getN_select_channel2;
    @FindBy(xpath = "//*[@id=\"addChannel\"]/div/div/div[2]/div[2]/button")
    WebElement n_btn_add;

    @FindBy(xpath = "/html/body/div[2]/div/div[2]/div/div/div[2]/button[2]")
    WebElement na_btn_new_ads;
    @FindBy(xpath = "//*[@id=\"addAdvertisement\"]/div/div/div[1]/h4/p")
    WebElement na_verify_new_ad_page;
    @FindBy(xpath = "//*[@id=\"21\"]")
    WebElement na_ad1;
    @FindBy(xpath = "//*[@id=\"23\"]")
    WebElement na_ad2;
    @FindBy(xpath = "//*[@id=\"tbl_all_advert_next\"]")
    WebElement na_btn_next;
    @FindBy(xpath = "//*[@id=\"tbl_all_advert_previous\"]")
    WebElement na_btn_previous;
    @FindBy(xpath = "//*[@id=\"tbl_all_advert_paginate\"]/span/a[2]")
    WebElement na_btn_2;
    @FindBy(xpath = "//*[@id=\"addAdvertisement\"]/div/div/div[2]/div[3]/button")
    WebElement na_btn_add;


    public AutoPlayList(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void clickzeroAds() {
        jse = (JavascriptExecutor) driver;
        jse.executeScript("arguments[0].click()", zero_ads);
    }

    public void setAuto_play() {
        jse = (JavascriptExecutor) driver;
        jse.executeScript("arguments[0].click()", auto_play);
    }

    public void setVerify_auto_playList() {
        copy_channel.isDisplayed();
    }

    public void deleteAds() {
        jse = (JavascriptExecutor) driver;
        jse.executeScript("arguments[0].click()", delete_ads);
//        driver.switchTo().alert().accept();
    }

    public void setCopy_channel() {
        wait = new WebDriverWait(driver, 10);
        WebElement c_channel = wait.until(ExpectedConditions.elementToBeClickable(copy_channel));
        c_channel.click();

    }

    public void verify_copy_channel() throws InterruptedException {
        Thread.sleep(1000);
        verify_copy_channel.isDisplayed();
    }

    public void copied_channel() {
        jse = (JavascriptExecutor) driver;
        jse.executeScript("arguments[0].click()", dd_copy_channel);
        select_channel.click();
    }

    public void to_channel() {
        jse = (JavascriptExecutor) driver;
        jse.executeScript("arguments[0].click()", copy_channel_to);
        select_channel2.click();
    }

    public void copied_button() {
        jse = (JavascriptExecutor) driver;
        jse.executeScript("arguments[0].click()", btn_copied);
        driver.switchTo().alert().accept();
    }

    public void click_new_channel() {
        jse = (JavascriptExecutor) driver;
        jse.executeScript("arguments[0].click()", btn_new_channel);
    }

    public void verify_new_channel() {
        n_verify_new_channel.isDisplayed();
    }

    public void n_select_new_channels() {
        jse = (JavascriptExecutor) driver;
        jse.executeScript("arguments[0].click()", n_select_channel1);
        jse.executeScript("arguments[0].click()", n_select_channel1);
        jse.executeScript("arguments[0].click()", getN_select_channel2);
    }

    public void n_add_button() {
        jse = (JavascriptExecutor) driver;
        jse.executeScript("arguments[0].click()", n_btn_add);
    }

    public void click_new_ads() {
        jse = (JavascriptExecutor) driver;
        jse.executeScript("arguments[0].click()", na_btn_new_ads);
    }

    public void verify_new_ads_page() {
        na_verify_new_ad_page.isDisplayed();
    }

    public void click_ads() throws InterruptedException {
        jse = (JavascriptExecutor) driver;
        jse.executeScript("arguments[0].click()", na_ad1);
        Thread.sleep(1000);
        jse.executeScript("arguments[0].click()", na_ad1);
        Thread.sleep(1000);
        jse.executeScript("arguments[0].click()", na_ad2);
        Thread.sleep(1000);
        jse.executeScript("arguments[0].click()", na_btn_next);
        Thread.sleep(1000);
        jse.executeScript("arguments[0].click()", na_btn_previous);
        Thread.sleep(1000);
        jse.executeScript("arguments[0].click()", na_btn_2);

    }

    public void btn_click_add() throws InterruptedException {
        Thread.sleep(1000);
        jse = (JavascriptExecutor) driver;
        jse.executeScript("arguments[0].click()", na_btn_add);
    }

}
