package pages.scheduler;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.security.cert.X509Certificate;

public class Tags {

    protected WebDriver driver;
    protected WebDriverWait wait;
    protected JavascriptExecutor jse;

    @FindBy(xpath = "//*[@id=\"includedMenubar\"]/nav[2]/div/ul/li[5]/a")
    WebElement tab_tags;
    @FindBy(xpath = "//*[@id=\"all-advert-main-div\"]/div/div[1]/p")
    WebElement verify_tags;

    @FindBy(xpath = "//*[@id=\"all_advertisement_table_next\"]")
    WebElement _next;
    @FindBy(xpath = "//*[@id=\"all_advertisement_table_previous\"]")
    WebElement _previous;
    @FindBy(xpath = "//*[@id=\"all_advertisement_table_paginate\"]/span/a[2]")
    WebElement forward_2;

    @FindBy(xpath = "//*[@id=\"13852\"]")
    WebElement btn_tags;
    @FindBy(xpath = "//*[@id=\"channel_select\"]/div/div/button/span[1]")
    WebElement channel;

    public Tags(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void navigate_tags() {
        jse = (JavascriptExecutor) driver;
        jse.executeScript("arguments[0].click()", tab_tags);
    }

    public void verify_tags() {
        verify_tags.isDisplayed();
    }

    public void entries() throws InterruptedException {
        Thread.sleep(1000);
        _next.click();
        Thread.sleep(1000);
        _previous.click();
        Thread.sleep(1000);
        forward_2.click();
    }

    public void click_tag() {
        jse = (JavascriptExecutor) driver;
        jse.executeScript("arguments[0].click()", btn_tags);
    }

    public void click_channel() {
        wait = new WebDriverWait(driver, 10);
        WebElement _channel = wait.until(ExpectedConditions.elementToBeClickable(channel));
        _channel.click();
    }


}
