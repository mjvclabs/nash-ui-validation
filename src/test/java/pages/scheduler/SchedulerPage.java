package pages.scheduler;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SchedulerPage {

    protected WebDriver driver;
    protected WebDriverWait wait;
    protected JavascriptExecutor jse;
    protected Select select;

    @FindBy(how = How.XPATH, using = "/html/body/div/div[2]/div[2]/div/div/div/a[2]/div/div/p")
    WebElement click_scheduler;
    @FindBy(how = How.XPATH, using = "//*[@id=\"schedule-home-main-div\"]/div[2]/div[1]/p")
    WebElement verify_scheduler_page;
    @FindBy(linkText = "Scheduler")
    WebElement btn_scheduler_page;
    private By select_advert = By.linkText("6025"); //selected advert
    @FindBy(xpath = "//*[@id=\"new_work_order_div\"]/div[2]/div/div[1]/button")
    WebElement btn_addButton;
    private By txt_advert_list_pop = By.xpath("//*[@id=\"advert_list_pop_div\"]/div/button/span[1]");
    private By txt_click_serached_advert = By.xpath("//*[@id=\"advert_list_pop_div\"]/div/div/ul/li[6]/a/span[1]");
    @FindBy(xpath = "//*[@id=\"addAdvertisementModal\"]/div/div/div[2]/div/div[2]/button")
    WebElement btn_add_button_advert_list;

    @FindBy(xpath = "//*[@id=\"5\"]")
    WebElement btn_delete;
    @FindBy(xpath = "//*[@id=\"addAdvertisementModal\"]/div/div/div[3]/button")
    WebElement btn_save_advert;

    @FindBy(xpath = "//*[@id=\"toggle_advetlist_collapse\"]")
    WebElement down_arrow_toggle_advetlist_collapse;
    @FindBy(xpath = "//*[@id=\"advert_main_table\"]/thead/tr/th[2]")
    WebElement tbl_advet_tableDisplyed;

    @FindBy(xpath = "//*[@id=\"search-from-date\"]/div/span/span")
    WebElement date_range_from_element;
    @FindBy(xpath = "/html/body/div[5]/div/div[1]/table/tbody/tr[4]/td[5]")
    WebElement dte_from;
    @FindBy(xpath = "//*[@id=\"search-to-date\"]/div/span/span")
    WebElement date_range_to_element;
    @FindBy(xpath = "/html/body/div[6]/div/div[1]/table/tbody/tr[6]/td[1]")
    WebElement dte_to;
    @FindBy(xpath = "//*[@id=\"go-btn\"]")
    WebElement btn_go;

    @FindBy(xpath = "//*[@id=\"tbleft_0\"]/td[1]/input")
    WebElement checkAdvertisment;

    @FindBy(xpath = "//*[@id=\"new_work_order_div\"]/div[2]/div/div[2]/button")
    WebElement btn_replace;
    @FindBy(xpath = "//*[@id=\"replaceAdvertisementModal\"]/div/div/div[1]/p")
    WebElement verify_advert_replacement_page;

    @FindBy(id = "rd_rp_vertical")
    WebElement ra_btn_allSchedule;
    @FindBy(id = "rd_rp_horizontal")
    WebElement ra_btn_selectedSchedule;
    @FindBy(xpath = "//*[@id=\"original_advertDropdown_pop\"]")
    WebElement dd_select_advert;
    @FindBy(xpath = "//*[@id=\"original_advert_list_pop_div\"]/span[1]/div/button/span")
    WebElement channel;
    @FindBy(xpath = "//*[@id=\"original_advert_list_pop_div\"]/span[1]/div/ul/li/a/label/input")
    WebElement select_channel;
    @FindBy(xpath = "//*[@id=\"rp_form-date\"]/div/span/span") //date range in advertisment replacement window(from)
    WebElement select_date_from;
    @FindBy(xpath = "/html/body/div[4]/div/div[1]/table/tbody/tr[4]/td[1]")
    WebElement select_date_f;
    @FindBy(xpath = "//*[@id=\"rp_to-date\"]/div/span/span") //date range in advertisment replacement window(To)
    WebElement select_date_to;
    @FindBy(xpath = "/html/body/div[7]/div/div[1]/table/tbody/tr[5]/td[6]")
    WebElement select_date_t;
    @FindBy(id = "rd_rp_vertical")
    WebElement pattern_vertical;
    @FindBy(id = "rd_rp_horizontal")
    WebElement pattern_horizontal;
    @FindBy(id = "replace_advertDropdown_pop")
    WebElement replace_with;
    @FindBy(xpath = "//*[@id=\"replace_advertDropdown_pop\"]/option[5]")
    WebElement replace_with_advertisment;
    @FindBy(xpath = "//*[@id=\"replaceAdvertisementModal\"]/div/div/div[2]/div/form/div[6]/div[2]/button")
    WebElement btn_add_channel_replacement;
    @FindBy(xpath = "//*[@id=\"replaceAdvertisementModal\"]/div/div/div[2]/div/form/div[9]/button")
    WebElement replace_advertisment;
    @FindBy(xpath = "/html/body/div[9]/div[2]/div[2]/button[1]")
    WebElement btn_continue;
    @FindBy(xpath = "/html/body/div[9]/div[2]/div[2]/button[2]")
    WebElement btn_cancel;
    @FindBy(xpath = "//*[@id=\"replaceAdvertisementModal\"]/div/div/div[1]/button")
    WebElement close_replacement_window;


    public SchedulerPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void setClick_scheduler() {
        click_scheduler.click();
    }

    public void setVerify_scheduler_page() {
        verify_scheduler_page.isDisplayed();
    }

    public void setBtn_scheduler_page() {
        btn_scheduler_page.click();
    }

//    public void setLink_workOrder_element() {
//       wait = new WebDriverWait(driver, 10);
//       WebElement selected_advert = wait.until(ExpectedConditions.elementToBeClickable(select_advert));
//       selected_advert.click();
//    }

    public void setBtn_addButton() {
        btn_addButton.click();
    }

    public void setTxt_advert_list_pop() {
        wait = new WebDriverWait(driver, 10);
        WebElement advert_list_pop = wait.until(ExpectedConditions.elementToBeClickable(txt_advert_list_pop));
        advert_list_pop.click();
    }

    public void setAdvertismennt() {
        wait = new WebDriverWait(driver, 10);
        WebElement click_searched_advert = wait.until(ExpectedConditions.elementToBeClickable(txt_click_serached_advert));
        click_searched_advert.click();

    }

    public void addSelectedAdvert() {
        btn_add_button_advert_list.click();
    }

    public void deleteAdvert() {
        btn_delete.click();
        driver.switchTo().alert().accept();
    }

    public void save_button() {
        btn_save_advert.click();
    }

    public void toggle_advetlist_collapse() {
        jse = (JavascriptExecutor) driver;
        jse.executeScript("arguments[0].click()", down_arrow_toggle_advetlist_collapse);
    }

    public void advertTableDisplayed() {
        tbl_advet_tableDisplyed.isDisplayed();
    }

    public void dateRangeFrom() {
        jse = (JavascriptExecutor) driver;
        jse.executeScript("arguments[0].click()", date_range_from_element);
        dte_from.click();
    }

    public void dateRangeTo() {
        jse = (JavascriptExecutor) driver;
        jse.executeScript("arguments[0].click()", date_range_to_element);
        dte_to.click();
    }

    public void btn_click_go() {
        jse = (JavascriptExecutor) driver;
        jse.executeScript("arguments[0].click()", btn_go);
    }

    public void setCheckAdvertisment() {
        jse = (JavascriptExecutor) driver;
        jse.executeScript("arguments[0].click()", checkAdvertisment);
    }

    public void clickReplaceBtn() {
        wait = new WebDriverWait(driver, 10);
        WebElement replace = wait.until(ExpectedConditions.elementToBeClickable(btn_replace));
        replace.click();
    }

    public void replacementpage() {
        verify_advert_replacement_page.isDisplayed();
    }

    public void replace_advert() {
        jse = (JavascriptExecutor) driver;
        jse.executeScript("arguments[0].click()", ra_btn_allSchedule);
        jse = (JavascriptExecutor) driver;
        jse.executeScript("arguments[0].click()", ra_btn_selectedSchedule);
        select = new Select(dd_select_advert);
        select.selectByValue("18911");

    }

    public void select_channel() {
        jse = (JavascriptExecutor) driver;
        jse.executeScript("arguments[0].click()", channel);
//        jse = (JavascriptExecutor)driver;
        jse.executeScript("arguments[0].click()", select_channel);
    }

    public void advert_replacement_date_range() throws InterruptedException {
        jse = (JavascriptExecutor) driver;
        jse.executeScript("arguments[0].click()", select_date_from);
        select_date_f.click();
        Thread.sleep(2000);
        jse = (JavascriptExecutor) driver;
        jse.executeScript("arguments[0].click()", select_date_to);
        select_date_t.click();
    }

    public void select_pattern() throws InterruptedException {
        wait = new WebDriverWait(driver, 10);
        WebElement vertical = wait.until(ExpectedConditions.elementToBeClickable(pattern_vertical));
        vertical.click();
        Thread.sleep(1000);
        WebElement horizontal = wait.until(ExpectedConditions.elementToBeClickable(pattern_horizontal));
        horizontal.click();
    }

    public void replace_with_channel() throws InterruptedException {
        wait = new WebDriverWait(driver, 10);
        WebElement replaceChannel = wait.until(ExpectedConditions.elementToBeClickable(replace_with));
        replaceChannel.click();
        Thread.sleep(1000);
        WebElement replaceAdvert = wait.until(ExpectedConditions.elementToBeClickable(replace_with_advertisment));
        replaceAdvert.click();
    }

    public void add_btn_replace_channel() throws InterruptedException {
        Thread.sleep(1000);
        btn_add_channel_replacement.click();
    }

    public void btn_replace_advert() throws InterruptedException {
        Thread.sleep(1000);
        replace_advertisment.click();
    }

    public void setBtn_continue() {
        wait = new WebDriverWait(driver, 10);
        WebElement _continue = wait.until(ExpectedConditions.elementToBeClickable(btn_continue));
        _continue.click();
    }

    public void close_replace_window() throws InterruptedException {
        Thread.sleep(3000);
        close_replacement_window.click();
    }
}
