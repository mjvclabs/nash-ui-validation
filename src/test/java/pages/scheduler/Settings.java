package pages.scheduler;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Settings {

    protected WebDriver driver;
    protected WebDriverWait wait;
    protected JavascriptExecutor jse;

    @FindBy(xpath = "//*[@id=\"includedMenubar\"]/nav[2]/div/ul/li[4]/a")
    WebElement zero_ads;
    @FindBy(xpath = "//*[@id=\"includedMenubar\"]/nav[2]/div/ul/li[4]/ul/li[3]/a")
    WebElement settings;
    @FindBy(xpath = "//*[@id=\"18_manual\"]")
    WebElement advert_1;
    @FindBy(xpath = "//*[@id=\"21_auto\"]")
    WebElement advert_2;
    @FindBy(xpath = "/html/body/div[2]/div/div[2]/div/div/div[2]/button")
    WebElement btn_save;

    public Settings(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void clickZeroAds() {
        jse = (JavascriptExecutor) driver;
        jse.executeScript("arguments[0].click()", zero_ads);
    }

    public void clickSettings() {
        jse = (JavascriptExecutor) driver;
        jse.executeScript("arguments[0].click()", settings);
    }

    public void check_advertisements() {
        wait = new WebDriverWait(driver, 10);
        WebElement advert1 = wait.until(ExpectedConditions.elementToBeClickable(advert_1));
        advert1.click();
        WebElement advert2 = wait.until(ExpectedConditions.elementToBeClickable(advert_2));
        advert2.click();
    }

    public void save() {
        wait = new WebDriverWait(driver, 10);
        WebElement save = wait.until(ExpectedConditions.elementToBeClickable(btn_save));
        save.click();
    }

}
