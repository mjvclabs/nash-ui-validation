package pages.scheduler;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Collections;


public class PrioritySettings {

    protected WebDriver driver;
    protected WebDriverWait wait;
    protected JavascriptExecutor jse;
    protected Select select;

    @FindBy(linkText = "Priority Setting")
    WebElement tab_click_priority;
    @FindBy(xpath = "//*[@id=\"channelDropdown\"]")
    WebElement dd_select_channel;
    @FindBy(xpath = "//*[@id=\"channelDropdown\"]/option[6]")
    WebElement choose_channel;
    @FindBy(xpath = "//*[@id=\"timebeltDropdown\"]")
    WebElement time_belt;
    @FindBy(xpath = "//*[@id=\"timebeltDropdown\"]/option[8]")
    WebElement select_time_belt;
    @FindBy(xpath = "//*[@id=\"clusterDropdown\"]")
    WebElement _cluster;
    @FindBy(xpath = "//*[@id=\"clusterDropdown\"]/option[2]")
    WebElement select_cluster;

    @FindBy(xpath = "//*[@id=\"divFromDate\"]/span[2]")
    WebElement select_date_from;
    @FindBy(xpath = "/html/body/div[6]/div/div[1]/table/tbody/tr[3]/td[6]")
    WebElement _from;
    @FindBy(xpath = "//*[@id=\"divToDate\"]/span[2]")
    WebElement select_date_to;
    @FindBy(xpath = "/html/body/div[7]/div/div[1]/table/tbody/tr[6]/td[1]")
    WebElement _to;

    @FindBy(xpath = "//*[@id=\"ckbx_Mon\"]")
    WebElement monday;
    @FindBy(xpath = "//*[@id=\"ckbx_Tue\"]")
    WebElement tuesday;
    @FindBy(xpath = "//*[@id=\"ckbx_Wed\"]")
    WebElement wednesday;
    @FindBy(xpath = "//*[@id=\"ckbx_Thu\"]")
    WebElement thursday;
    @FindBy(xpath = "//*[@id=\"ckbx_Fri\"]")
    WebElement friday;
    @FindBy(xpath = "//*[@id=\"ckbx_Sat\"]")
    WebElement saturday;
    @FindBy(xpath = "//*[@id=\"ckbx_Sun\"]")
    WebElement sunday;
    @FindBy(xpath = "//*[@id=\"priority_form\"]/div[3]/div[2]/button")
    WebElement _filter;
    @FindBy(id = "workOrderDropdown")
    WebElement workOrder;
    @FindBy(id = "advertDropdown")
    WebElement dd_advert;
    @FindBy(id = "priorityDropdown")
    WebElement dd_priority;
    @FindBy(xpath = "//*[@id=\"ckbxOverWrite\"]")
    WebElement overwrite_existing_priorities;
    @FindBy(xpath = "//*[@id=\"priority_form\"]/div[5]/div[3]/button")
    WebElement btn_save;


    public PrioritySettings(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void click_priority_tab() {
        wait = new WebDriverWait(driver, 10);
        WebElement click_priority = wait.until(ExpectedConditions.elementToBeClickable(tab_click_priority));
        click_priority.click();
    }

    public void select_channel() {
        wait = new WebDriverWait(driver, 10);
        WebElement dd_click_channel = wait.until(ExpectedConditions.elementToBeClickable(dd_select_channel));
        dd_click_channel.click();
        WebElement ch_channel = wait.until(ExpectedConditions.elementToBeClickable(choose_channel));
        ch_channel.click();
    }

    public void select_time() {
        wait = new WebDriverWait(driver, 10);
        WebElement timeBelt = wait.until(ExpectedConditions.elementToBeClickable(time_belt));
        timeBelt.click();
        WebElement s_time_belt = wait.until(ExpectedConditions.elementToBeClickable(select_time_belt));
        s_time_belt.click();
    }

    public void select_cluster() {
        wait = new WebDriverWait(driver, 10);
        WebElement cluster = wait.until(ExpectedConditions.elementToBeClickable(_cluster));
        cluster.click();
        WebElement s_cluster = wait.until(ExpectedConditions.elementToBeClickable(select_cluster));
        s_cluster.click();
    }

    public void selectDate() throws InterruptedException {
        jse = (JavascriptExecutor) driver;
        jse.executeScript("arguments[0].click()", select_date_from);
        _from.click();
        Thread.sleep(2000);
        jse.executeScript("arguments[0].click()", select_date_to);
        _to.click();
    }

    public void apply_days_pattern() throws InterruptedException {
        Thread.sleep(1000);
        for (int i = 1; i <= 2; i++) {
            monday.click();
            Thread.sleep(1000);
            tuesday.click();
            Thread.sleep(1000);
            wednesday.click();
            Thread.sleep(1000);
            thursday.click();
            Thread.sleep(1000);
            friday.click();
            Thread.sleep(1000);
            saturday.click();
            Thread.sleep(1000);
            sunday.click();
            Thread.sleep(1000);
        }
    }

    public void apply_filter() throws InterruptedException {
        Thread.sleep(1000);
        _filter.click();
    }

    public void setWorkOrder() throws InterruptedException {
        Thread.sleep(1000);
        select = new Select(workOrder);
        select.selectByValue("6022");
    }

    public void setDd_advert() throws InterruptedException {
        Thread.sleep(1000);
        select = new Select(dd_advert);
        select.selectByVisibleText("Dialog Uber 20s Sin TVC");
    }

    public void priority_level() throws InterruptedException {
        Thread.sleep(1000);
        select = new Select(dd_priority);
        select.selectByVisibleText("Priority 2");
    }

    public void overwriteExistingPriorities() throws InterruptedException {
        Thread.sleep(1000);
        overwrite_existing_priorities.click();
    }

    public void save() throws InterruptedException {
        Thread.sleep(1000);
        btn_save.click();
    }



}