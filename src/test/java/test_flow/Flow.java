package test_flow;

import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.jupiter.api.AfterEach;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.IExecutionListener;
import pages.LoginPage;
import pages.WorkOrderPage;
import pages.scheduler.*;

import java.net.InetAddress;
import java.net.URL;


public class Flow implements IExecutionListener {

    private WebDriver driver;
    DesiredCapabilities capabilities = new DesiredCapabilities();

    private LoginPage loginPage;
    private WorkOrderPage workOrderPage;
    private SchedulerPage schedulerPage;
    private PrioritySettings prioritySettings;
    private AutoPlayList autoPlayList;
    private ManualPlayList manualPlayList;
    private Settings settings;
    private Tags tags;
    protected static ThreadLocal<RemoteWebDriver> _driver = new ThreadLocal<>();
    public static String remote_url_chrome = "http://selenium-hub:4444/wd/hub";
//    WebDriverManager wdm = WebDriverManager.chromedriver().browserInDocker().enableVnc().enableRecording();


    @Before
    public void browser_is_open() {

        ChromeOptions options = new ChromeOptions();
        try {
            options.addArguments("--disable-infobars");
            options.addArguments("--headless");
            options.addArguments("--no-sandbox");
            options.addArguments("--disable-dev-shm-usage");
            options.addArguments("--ignore-certificate-errors");
            options.addArguments("--blink-settings=imagesEnabled=false");
            options.addArguments("--disable-gpu");
            options.addArguments("--add-host host.docker.internal:host-gateway");
            options.addArguments("--start-maximized");
            options.addArguments("--disable-web-security");
            options.addArguments("--disable-browser-side-navigation");
            options.addArguments("--disable-notifications");
            options.addArguments("--remote-debugging-port=9222");
            options.addArguments("--disable-extensions");
            options.addArguments("--allow-running-insecure-content");
            options.addArguments("--allow-insecure-localhost");
            options.addArguments("--window-size=1024,768");
            System.setProperty("webdriver.chrome.args", "--disable-logging");
            System.setProperty("webdriver.chrome.silentOutput", "true");
            options.setPageLoadStrategy(PageLoadStrategy.NONE);
            options.setExperimentalOption("useAutomationExtension", false);

            capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
            capabilities.setCapability(ChromeOptions.CAPABILITY, options);

            InetAddress ia = InetAddress.getLocalHost();
            String str = ia.getHostAddress();
            System.out.println(str);
            System.out.println(remote_url_chrome);

//            driver = new RemoteWebDriver(new URL(remote_url_chrome), options);
            _driver.set(new RemoteWebDriver(new URL(remote_url_chrome), options));
            driver = getDriver();
            driver.manage().window().maximize();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public WebDriver getDriver()
    {
        return _driver.get();
    }


    @AfterEach
    public void tearDownDriver()
    {
        getDriver().quit();
    }


//    @AfterEach
//    public void closeBrowser() {
//        driver.close();
//        driver.quit();
//    }

    @Given("user is on login page")
    public void user_is_on_login_page() throws InterruptedException {
//        driver.navigate().to("https://opensource-demo.orangehrmlive.com/");
        Thread.sleep(10000);
        driver.get("http://app:6868/signin");
        driver.manage().window().maximize();
        System.out.println("session started");
        Thread.sleep(15000);
        System.out.println(driver.getTitle());
        Thread.sleep(5000);
    }


    @When("user enters username and password")
    public void user_enters_username_and_password() throws InterruptedException {
        Thread.sleep(15000);
        loginPage = new LoginPage(driver);
        Thread.sleep(5000);
        loginPage.enterUserName("SU");

        Thread.sleep(5000);
        loginPage.enterPassword("user");
    }

    @And("user click on login")
    public void user_click_on_login() throws InterruptedException {
        Thread.sleep(3000);
        loginPage.clickLogin();
    }

    @Then("navigate to homepage")
    public void navigate_to_homepage() throws InterruptedException {
        Thread.sleep(30000);
        loginPage.checkTestCase();
    }


    //// WorkOrder Module Section////
    @When("user clicks to work order tab")
    public void user_clicks_to_work_order_tab() throws InterruptedException {
        Thread.sleep(1000);
        workOrderPage = new WorkOrderPage(driver);
        workOrderPage.navigateWorkOrderPage();
    }

    @Then("work orders table displays")
    public void work_orders_table_displays() {
        workOrderPage.checkTable();
    }

    @Given("user click new work order tab")
    public void user_click_new_work_order_tab() throws InterruptedException {
        Thread.sleep(1000);
        workOrderPage.clickNewWorkOrder();
    }

    @And("user is on new work order page")
    public void user_is_on_new_work_order_page() throws InterruptedException {
        Thread.sleep(2000);
        workOrderPage.verify_work_order_page();
    }

    @And("user select marketing ex")
    public void user_select_marketing_ex() throws InterruptedException {
        Thread.sleep(4000);
        workOrderPage.marketing_ex();
    }

    @And("user select WO type")
    public void user_select_wo_type() throws InterruptedException {
        Thread.sleep(1000);
        workOrderPage.wo_type();
    }

    @And("user enters schedule ref")
    public void user_enters_schedule_ref() throws InterruptedException {
        Thread.sleep(1000);
        workOrderPage.schduleRef();
    }

    @And("user enters product")
    public void user_enters_product() throws InterruptedException {
        Thread.sleep(1000);
        workOrderPage.productName();
    }

    @And("user enters invoice type")
    public void user_enters_invoice_type() throws InterruptedException {
    }

    @And("user select agency")
    public void user_select_agency() throws InterruptedException {
        Thread.sleep(1000);
        workOrderPage.agency();
    }

    @And("user select end client")
    public void user_select_end_client() throws InterruptedException {
        Thread.sleep(1000);
        workOrderPage.endClient();
    }

    @And("user select billing client")
    public void user_select_billing_client() throws InterruptedException {
        Thread.sleep(1000);
        workOrderPage.billClient();
    }

    @And("user select time period")
    public void user_select_time_period() throws InterruptedException {
        Thread.sleep(1000);
        workOrderPage.fromDate();
        Thread.sleep(2000);
        workOrderPage.toDate();
    }

    @And("user select revenue month")
    public void user_select_revenue_month() throws InterruptedException {
        Thread.sleep(2000);
        workOrderPage.revenueMonth();
    }

    @And("user enters total budget")
    public void user_enters_total_budget() throws InterruptedException {
        Thread.sleep(2000);
        workOrderPage.totalBudget();
        Thread.sleep(2000);
        workOrderPage.saveBudget();
    }

    @And("user add the advertisement to work order")
    public void user_add_the_advertisement_to_work_order() throws InterruptedException {
        workOrderPage.addAdvertisment();
    }

    @And("user enters the comment")
    public void user_enters_the_comment() throws InterruptedException {
        Thread.sleep(2000);
        workOrderPage.addComments();
    }

    @And("user select the channel")
    public void user_select_the_channel() throws InterruptedException {
        Thread.sleep(2000);
        workOrderPage.channelDropdown();

    }

    @And("user give to click bonus")
    public void user_give_to_click_bonus() throws InterruptedException {
        Thread.sleep(2000);
        workOrderPage.check_bonus();
    }

    @And("user click tv commercials")
    public void user_click_tv_commercials() throws InterruptedException {
        Thread.sleep(1000);
        workOrderPage.click_tvc();
        Thread.sleep(2000);
        workOrderPage.tvc_duration();
        Thread.sleep(1000);
        workOrderPage.tvc_spots();
        Thread.sleep(1000);
        workOrderPage.tvc_addBtn();
        Thread.sleep(1000);
        workOrderPage.tvc_saveBtn();
    }

    @And("user add values to tvc")
    public void user_add_values_to_tvc() throws InterruptedException {
        Thread.sleep(2000);
        workOrderPage.setTxt_tvc_value();
    }

    @And("user add some bonus tvc")
    public void user_add_some_bonus_tvc() throws InterruptedException {
        Thread.sleep(2000);
        workOrderPage.addBonusTVC();
    }

    @And("user add logo section")
    public void user_add_logo_section() throws InterruptedException {
        Thread.sleep(2000);
        workOrderPage.setTxt_logo_spots();
        Thread.sleep(2000);
        workOrderPage.setTxt_logo_spots_b();
        Thread.sleep(2000);
        workOrderPage.setTxt_logos_spots_packege();
    }

    @And("user add Crawler section")
    public void user_add_crawler_section() throws InterruptedException {
        Thread.sleep(2000);
        workOrderPage.setTxt_Crawler_spots();
        Thread.sleep(2000);
        workOrderPage.setTxt_Crawler_spots_b();
        Thread.sleep(2000);
        workOrderPage.setTxt_crawler_spots_packege();
    }

    @And("user add Vsqueeze section")
    public void user_add_vsqueeze_section() throws InterruptedException {
        Thread.sleep(2000);
        workOrderPage.setTxt_v_squeeze_spots();
        Thread.sleep(2000);
        workOrderPage.setTxt_v_squeeze_spots_b();
        Thread.sleep(2000);
        workOrderPage.setTxt_v_squeeze_spots_packege();
    }

    @And("user add Lsqueeze section")
    public void user_add_lsqueeze_section() throws InterruptedException {
        Thread.sleep(2000);
        workOrderPage.setTxt_l_squeeze_spots();
        Thread.sleep(2000);
        workOrderPage.setTxt_l_squeeze_spots_b();
        Thread.sleep(2000);
        workOrderPage.setTxt_l_squeeze_spots_packege();
    }

    @And("user add channel details and save")
    public void user_add_channel_details_and_save() throws InterruptedException {
        Thread.sleep(2000);
        workOrderPage.setBtn_add_divChannelDetails();
        Thread.sleep(2000);
        workOrderPage.setBtn_save_workOrder();
    }

    //// Scheduler Page ////

    @When("user clicks to scheduler tab")
    public void user_clicks_to_scheduler_tab() {
        schedulerPage = new SchedulerPage(driver);
        schedulerPage.setClick_scheduler();
    }

    @Then("user on scheduler page")
    public void user_on_scheduler_page() {
        schedulerPage.setVerify_scheduler_page();
    }

    @Given("user click scheduler tab")
    public void user_click_scheduler_tab() {
        schedulerPage.setBtn_scheduler_page();
    }

    @And("user click the current work table element")
    public void user_click_the_current_work_table_element() throws InterruptedException {
        Thread.sleep(2000);
        driver.navigate().to("http://localhost:8080/schedule/current-schedule?work_id=6025&work_name=1002&start_date=2021-10-21&end_date=2021-10-31");
//        Thread.sleep(2000);
//        schedulerPage.setLink_workOrder_element();
        Thread.sleep(5000);
    }

    @When("user click add button on the scheduler page")
    public void user_click_add_button_on_the_scheduler_page() throws InterruptedException {
        Thread.sleep(1000);
        schedulerPage.setBtn_addButton();
    }

    @And("user click the select advertisement")
    public void user_click_the_select_advertisement() throws InterruptedException {
        Thread.sleep(2000);
        schedulerPage.setTxt_advert_list_pop();
        Thread.sleep(2000);
        schedulerPage.setAdvertismennt();
        Thread.sleep(1000);
        schedulerPage.addSelectedAdvert();
        Thread.sleep(1000);
        schedulerPage.deleteAdvert();
        Thread.sleep(1000);
        schedulerPage.save_button();
    }

    @When("user clicks down arrow button to show the advertisement")
    public void user_clicks_down_arrow_button_to_show_the_advertisement() throws InterruptedException {
        Thread.sleep(6000);
        schedulerPage.toggle_advetlist_collapse();
    }

    @Then("show the advertisement table")
    public void show_the_advertisement_table() throws InterruptedException {
        Thread.sleep(1000);
        schedulerPage.advertTableDisplayed();
    }

    @And("user select the date range")
    public void user_select_the_date_range() throws InterruptedException {
        Thread.sleep(6000);
        schedulerPage.dateRangeFrom();
        Thread.sleep(1000);
        schedulerPage.dateRangeTo();
        Thread.sleep(1000);
        schedulerPage.btn_click_go();
    }

    @And("user check channel to replace")
    public void user_check_channel_to_replace() throws InterruptedException {
        Thread.sleep(1000);
        schedulerPage.setCheckAdvertisment();
    }

    @When("user click the replace button")
    public void user_click_the_replace_button() throws InterruptedException {
        Thread.sleep(1000);
        schedulerPage.clickReplaceBtn();
    }

    @Then("popup the advertisement replace window")
    public void popup_the_advertisement_replace_window() {
        schedulerPage.replacementpage();
    }

    @And("user replace the advertisement")
    public void user_replace_the_advertisement() throws InterruptedException {
        schedulerPage.replace_advert();
        Thread.sleep(3000);
        schedulerPage.select_channel();
        Thread.sleep(1000);
        schedulerPage.advert_replacement_date_range();
        Thread.sleep(1000);
        schedulerPage.select_pattern();
        Thread.sleep(1000);
        schedulerPage.replace_with_channel();
        Thread.sleep(1000);
        schedulerPage.add_btn_replace_channel();
        Thread.sleep(1000);
        schedulerPage.btn_replace_advert();
        Thread.sleep(2000);
        schedulerPage.setBtn_continue();
        Thread.sleep(3000);
        schedulerPage.close_replace_window();
    }

    @When("user click the priority setting")
    public void user_click_the_priority_setting() throws InterruptedException {
        prioritySettings = new PrioritySettings(driver);
        Thread.sleep(2000);
        prioritySettings.click_priority_tab();
        Thread.sleep(2000);
        prioritySettings.select_channel();
        Thread.sleep(2000);
        prioritySettings.select_time();
        Thread.sleep(2000);
        prioritySettings.select_cluster();
        Thread.sleep(1000);
        prioritySettings.selectDate();
        Thread.sleep(1000);
        prioritySettings.apply_days_pattern();
        Thread.sleep(1000);
        prioritySettings.apply_filter();
        Thread.sleep(1000);
        prioritySettings.setWorkOrder();
        Thread.sleep(1000);
        prioritySettings.setDd_advert();
        Thread.sleep(1000);
        prioritySettings.priority_level();
        Thread.sleep(1000);
        prioritySettings.overwriteExistingPriorities();
        Thread.sleep(1000);
        prioritySettings.save();
    }

    @And("user clicks the zero ads")
    public void user_clicks_the_zero_ads() throws InterruptedException {
        autoPlayList = new AutoPlayList(driver);
        Thread.sleep(1000);
        autoPlayList.clickzeroAds();
        Thread.sleep(1000);
        autoPlayList.setAuto_play();
    }

    @Then("verify the auto auto playlist section")
    public void verify_the_the_auto_auto_playlist_section() throws InterruptedException {
        Thread.sleep(2000);
        autoPlayList.setVerify_auto_playList();
    }

    @And("user delete the advertisment")
    public void user_delete_the_advertisment() throws InterruptedException {
        Thread.sleep(2000);
        autoPlayList.deleteAds();
    }

    @And("user clicks the copy channel")
    public void user_clicks_the_copy_channel() throws InterruptedException {
        Thread.sleep(1000);
        autoPlayList.setCopy_channel();
    }

    @Then("verify the copy channel window")
    public void verify_the_copy_channel_window() throws InterruptedException {
        Thread.sleep(1000);
        autoPlayList.verify_copy_channel();
    }

    @And("user clicks copy the channel")
    public void user_clicks_copy_the_channel() throws InterruptedException {
        Thread.sleep(1000);
        autoPlayList.copied_channel();
        Thread.sleep(1000);
        autoPlayList.to_channel();
    }

    @When("user clicks the copy button")
    public void user_clicks_the_copy_button() throws InterruptedException {
        Thread.sleep(1000);
        autoPlayList.copied_button();
    }

    @And("user clicks the new channel")
    public void user_clicks_the_new_channel() throws InterruptedException {
        Thread.sleep(1000);
        autoPlayList.click_new_channel();
    }

    @Then("verify the new channel window")
    public void verify_the_new_channel_window() throws InterruptedException {
        Thread.sleep(2000);
        autoPlayList.verify_new_channel();
    }

    @And("user select the channels")
    public void user_select_the_channels() throws InterruptedException {
        Thread.sleep(1000);
        autoPlayList.n_select_new_channels();
    }

    @And("user clicks the add button")
    public void user_clicks_the_add_button() throws InterruptedException {
        Thread.sleep(2000);
        autoPlayList.n_add_button();
    }

    @And("user clicks the new ads")
    public void user_clicks_the_new_ads() throws InterruptedException {
        Thread.sleep(2000);
        autoPlayList.click_new_ads();
    }

    @Then("verify the new ads window")
    public void verify_the_new_ads_window() throws InterruptedException {
        Thread.sleep(2000);
        autoPlayList.verify_new_ads_page();
    }

    @And("user add the new ads")
    public void user_ads_the_new_ads() throws InterruptedException {
        Thread.sleep(2000);
        autoPlayList.click_ads();
    }

    @And("user clicks the save")
    public void user_clicks_the_save() throws InterruptedException {
        Thread.sleep(2000);
        autoPlayList.btn_click_add();
    }

    @When("user clicks the manual playlist")
    public void user_clicks_the_manual_playlist() throws InterruptedException {
        manualPlayList = new ManualPlayList(driver);
        manualPlayList.clickZeroAds();
        Thread.sleep(2000);
        manualPlayList.manual_channel();

    }

    @Then("user verify the manual playlist")
    public void user_verify_the_manual_playlist() throws InterruptedException {
        Thread.sleep(1000);
        manualPlayList.setVerify_manual_playList();
    }

    @Then("user clicks the select channels")
    public void user_clicks_the_select_channels() throws InterruptedException {
        Thread.sleep(2000);
        manualPlayList.select_channel();
    }

    @Then("user clicks the add advertisment")
    public void user_clicks_the_add_advertisment() throws InterruptedException {
        Thread.sleep(1000);
        manualPlayList.add_advertisment();
    }

    @Then("user select the advertisment")
    public void user_select_the_advertisment() throws InterruptedException {
        Thread.sleep(2000);
        manualPlayList.select_advertisment();
        Thread.sleep(2000);
        manualPlayList.entries();
        Thread.sleep(2000);
        manualPlayList.addAdvertisment();
    }

    @Then("user delete the manual playlist advertisment")
    public void user_delete_the_manual_playlist_advertisment() throws InterruptedException {
        Thread.sleep(3000);
        manualPlayList.deleteAdvertisment();
    }

    @Then("user save the manual playlist")
    public void user_save_the_manual_playlist() throws InterruptedException {
        Thread.sleep(2000);
        manualPlayList.save();
    }

    @When("user clicks the settings")
    public void user_clicks_the_settings() throws InterruptedException {
        settings = new Settings(driver);
        Thread.sleep(2000);
        settings.clickZeroAds();
        Thread.sleep(1000);
        settings.clickSettings();
    }

    @Then("verify the setting tab")
    public void verify_the_setting_tab() {

    }

    @And("user select the appropriate channel list")
    public void user_select_the_appropriate_channel_list() throws InterruptedException {
        Thread.sleep(1000);
        settings.check_advertisements();
    }

    @And("user save the settings")
    public void user_save_the_settings() throws InterruptedException {
        Thread.sleep(1000);
        settings.save();
    }

    @When("user clicks the tag filler")
    public void user_clicks_the_tag_filler() throws InterruptedException {
        tags = new Tags(driver);
        Thread.sleep(1000);
        tags.navigate_tags();
    }

    @Then("verify the tag filler page")
    public void verify_the_tag_filler_page() throws InterruptedException {
        Thread.sleep(1000);
        tags.verify_tags();
    }

    @And("user clicks entries")
    public void user_clicks_entries() throws InterruptedException {
        Thread.sleep(1000);
        tags.entries();
    }

    @And("user click the tag button")
    public void user_click_the_tag_button() throws InterruptedException {
        Thread.sleep(1000);
        tags.click_tag();
    }

    @And("user select the channels from tags")
    public void user_select_the_channels_from_tags() throws InterruptedException {
    }

    @And("user save the tags")
    public void user_save_the_tags() {
    }


}