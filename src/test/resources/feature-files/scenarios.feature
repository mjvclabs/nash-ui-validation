Feature: Test Nash functionality

  Background: Validate login page is working
    Given user is on login page
    When user enters username and password
    And user click on login
    Then navigate to homepage

  Scenario: validate work order page is working
    When user clicks to work order tab
    Then work orders table displays
    Given user click new work order tab
    And user is on new work order page
    And user select marketing ex
    And user select WO type
    And user enters schedule ref
    And user enters product
    And user enters invoice type
    And user select agency
    And user select end client
    And user select billing client
    And user select time period
    And user select revenue month
    And user enters total budget
    And user add the advertisement to work order
    And user enters the comment
    And user select the channel
    And user give to click bonus
    And user click tv commercials
    And user add some bonus tvc
    And user add values to tvc
    And user add logo section
    And user add Crawler section
    And user add Vsqueeze section
    And user add Lsqueeze section
    And user add channel details and save

#  Scenario: validate schedule module is working
#    When user clicks to scheduler tab
#    Then user on scheduler page
#    Given user click scheduler tab
#    And user click the current work table element
#    When user click add button on the scheduler page
#    And user click the select advertisement
#    When user clicks down arrow button to show the advertisement
#    Then show the advertisement table
#    And user select the date range
#    And user check channel to replace
#    When user click the replace button
#    Then popup the advertisement replace window
#    And user replace the advertisement
#    When user click the priority setting
#    And user clicks the zero ads
#    Then verify the auto auto playlist section
#    And user delete the advertisment
#    And user clicks the copy channel
#    Then verify the copy channel window
#    And user clicks copy the channel
#    When user clicks the copy button
#    And user clicks the new channel
#    Then verify the new channel window
#    And user select the channels
#    And user clicks the add button
#    And user clicks the new ads
#    Then verify the new ads window
#    And user add the new ads
#    And user clicks the save
#    When user clicks the manual playlist
#    Then user verify the manual playlist
#    And user clicks the select channels
#    And user clicks the add advertisment
#    And user select the advertisment
#    And user delete the manual playlist advertisment
#    And user save the manual playlist
#    When user clicks the settings
#    Then verify the setting tab
#    And user select the appropriate channel list
#    And user save the settings
#    When user clicks the tag filler
#    Then verify the tag filler page
#    And user clicks entries
#    And user click the tag button
#    And user select the channels from tags
#    And user save the tags